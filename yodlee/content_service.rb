module Yodlee
  class ContentService < Base

    def login content_service_id
      return cobrand_token if cobrand_token['Error'].present?
      response = query({
        :endpoint => '/jsonsdk/ItemManagement/getLoginFormForContentService',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          :contentServiceId => content_service_id
        }
      })
    end

    def get_all_content_services(user_session_token)
      return cobrand_token if cobrand_token['Error'].present?
      return user_session_token if user_session_token['Error'].present?
      response = query({
        :endpoint => '/jsonsdk/ContentServiceTraversal/getAllContentServices',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token
        }
      })
    end

    def get_content_service_refresh_info(item_id, user_session_token)
      return cobrand_token if cobrand_token['Error'].present?
      return user_session_token if user_session_token['Error'].present?
      response = query({
        :endpoint => '/jsonsdk/Refresh/getRefreshInfo1',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token,
          "itemIds[0]" => item_id
        }
      })
    end

    def get_item_summaries_for_content_service(item_id, user_session_token)
      return cobrand_token if cobrand_token['Error'].present?
      return user_session_token if user_session_token['Error'].present?
      response = query({
        :endpoint => '/jsonsdk/DataService/getItemSummaryForItem1',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token,
          :itemId => 10157598,
          "dex.startLevel" => 0,
          "dex.endLevel" => 0,
          "dex.extentLevels[0]" => 4,
          "dex.extentLevels[1]" => 4
        }
      })
    end

    def start_refresh_container(itemId, user_session_token)
      return cobrand_token if cobrand_token['Error'].present?
      return user_session_token if user_session_token['Error'].present?
      query({
        :endpoint => '/jsonsdk/Refresh/startRefresh7',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token,
          :itemId => itemId,
          "refreshParameters.refreshMode.refreshMode" => 'NORMAL',
          "refreshParameters.refreshMode.refreshModeId" => 2,
          "refreshParameters.refreshPriority" => 1
        }
      })
    end

    def remove_content_service_account(item_id, user_session_token)
      return cobrand_token if cobrand_token['Error'].present?
      return user_session_token if user_session_token['Error'].present?
      response = query({
        :endpoint => '/jsonsdk/ItemManagement/removeItem',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token,
          :itemId => item_id
        }
      })
    end
    
  end
end