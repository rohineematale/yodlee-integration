module Yodlee
  class Form

    attr_reader :fields, :wrapper

    def initialize opts
      @fields  = opts[:fields]
      @wrapper = opts[:wrapper]
      @api_call = opts[:api_call]
    end

    def render
      arr = []
      fields.componentList.map do |element|
        if element.fieldInfoList
          element.fieldInfoList.map do |subelement|
            type = subelement.fieldType.typeName.downcase.classify
            arr << Yodlee::Fields.const_get(type).new(field: subelement, wrapper: "yodlee_account", api_call: @api_call).render
          end
        else
          type = element.fieldType.typeName.downcase.classify
          arr << Yodlee::Fields.const_get(type).new(field: element, wrapper: "yodlee_account", api_call: @api_call).render
        end
      end
      arr.join('').squish
    end

    def mfa_render
      arr = []
      if fields.fieldInfo.mfaFieldInfoType == "SECURITY_QUESTION"
        total_queAns = fields.fieldInfo.questionAndAnswerValues
        total_queAns.map do |queAns|
          queAns.each do |key, val|
            if (key == "question") || (key == "responseFieldType")
              wrapper = "yodlee_account[security_questions][#{queAns.metaData}]"
              hash = get_hashi_mash_hash(((key == "responseFieldType") ? "answerFieldType" : key), val)
              arr << Yodlee::Fields.const_get("Hidden").new(field: hash, wrapper: wrapper, api_call: @api_call).mfa_hidden_render
              if (key == "responseFieldType")
                type = queAns.responseFieldType.downcase.classify
                hash = get_hashi_mash_hash("answer", "", queAns.question)
                arr << Yodlee::Fields.const_get(type).new(field: hash, wrapper: wrapper, api_call: @api_call).render
              end
            end
          end
        end
        hash = get_hashi_mash_hash("mfa_type", "MFAQuesAns") if total_queAns.present?
        arr << Yodlee::Fields.const_get("Hidden").new(field: hash, wrapper: "yodlee_account", api_call: @api_call).mfa_hidden_render if total_queAns.present?

      elsif fields.fieldInfo.mfaFieldInfoType == "TOKEN_ID"
        type = fields.fieldInfo.responseFieldType.downcase.classify
        hash = get_hashi_mash_hash("token", "", fields.fieldInfo.displayString)
        arr << Yodlee::Fields.const_get(type).new(field: hash, wrapper: "yodlee_account", api_call: @api_call).render
        hash = get_hashi_mash_hash("mfa_type", "MFAToken")
        arr << Yodlee::Fields.const_get("Hidden").new(field: hash, wrapper: "yodlee_account", api_call: @api_call).mfa_hidden_render
      elsif fields.fieldInfo.mfaFieldInfoType == "IMAGE"
        type = fields.fieldInfo.responseFieldType.downcase.classify
        hash = get_hashi_mash_hash("image_text", "", fields.fieldInfo.displayString)
        arr << Yodlee::Fields.const_get(type).new(field: hash, wrapper: "yodlee_account", api_call: @api_call).render

        # binary_data = fields.fieldInfo.image.pack('C*')
        # rand_text = Time.now.to_i.to_s + rand.to_s[2..5]
        # IO.binwrite("#{Rails.root}/public/captcha/#{rand_text}.bmp", binary_data)
        # captcha = MfaCaptcha.create(:image => File.open("#{Rails.root}/public/captcha/#{rand_text}.bmp") )
        # `rm -rf #{Rails.root}/public/captcha/*`
        # hash.src = captcha.image_url
        
        hash = Hashie::Mash.new
        hash.src = fields.fieldInfo.image
        arr << Yodlee::Fields.const_get("Image").new(field: hash, wrapper: "yodlee_account", api_call: @api_call).mfa_hidden_render

        hash = get_hashi_mash_hash("mfa_type", "MFAImage")
        arr << Yodlee::Fields.const_get("Hidden").new(field: hash, wrapper: "yodlee_account", api_call: @api_call).mfa_hidden_render
      end
      arr.join('').squish
    end

    def get_hashi_mash_hash(identifier, val, display_name=nil)
      hash = Hashie::Mash.new
      hash.valueIdentifier = identifier
      hash.value = val
      hash.displayName = display_name
      return hash
    end

    def self.render_site_login_form(site_id, api_call=false)
      opts = {}
      response = Yodlee::Site.new.login(site_id)
      response['Error'] ? response : self.new(opts.merge({ fields: response, api_call: api_call })).render
    end

    def self.render_mfa_form(response, api_call=false)
      YodleeUtil.log_yodlee_responses("Action : Render MFA form started ")
      opts = {}
      res = response['Error'] ? response : self.new(opts.merge({ fields: response, api_call: api_call })).mfa_render
      YodleeUtil.log_yodlee_responses("Action : Render MFA form ended ")
      return res
    end

    def self.render_content_service_login_form(content_service_id)
      opts = {}
      response = Yodlee::ContentService.new.login(content_service_id)
      response['Error'] ? response : self.new(opts.merge({ fields: response })).render
    end

  end
end