module Yodlee
  class Importer < Base

    def add_yodlee_sites
      get_all_sites.each do |institute|
        mfa = institute.key?('mfaType') ? institute.mfaType.typeName : 'none'

        if institute.siteId
          row = YodleeFinancialInstitute.find_or_create_by(site_id: institute.siteId.to_i)

          row.update_attributes!(
            :home_url                     => institute.baseUrl,
            :site_id                      => institute.siteId,
            :site_original_name           => institute.defaultDisplayName,
            :site_org_display_name        => institute.defaultOrgDisplayName,
            :mfa                          => mfa
          )
          row.update_attributes!(:site_display_name => institute.defaultDisplayName) if !row.site_display_name.present?

          row.delay(:queue => 'site_logo', priority: 2).update_logo
          puts "Updated #{institute.defaultDisplayName}"
        end
      end
    end

    def get_all_sites
      sites = query({
        :endpoint => '/jsonsdk/SiteTraversal/getAllSites',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token
        }
      })
      puts "fetched all financial institutes"
      return sites
    end

    def get_popular_sites(username, password) # gives sites based on popular city, pincode, country or state
      token = current_user_token(username, password)
      if token['errorDetail'].present?
        return token
      else
        user_session_token = token[:session_token]
      end
      query({
        :endpoint => '/jsonsdk/SiteTraversal/getPopularSites',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token,
          "siteFilter.siteLevel" => "POPULAR_COUNTRY"
        }
      })
    end

    def get_site_info site_id
      query({
        :endpoint => '/jsonsdk/SiteTraversal/getSiteInfo',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          "siteFilter.reqSpecifier" => 128,
          "siteFilter.siteId" => site_id
        }
      })      
    end

    def search_site(username, password)
      token = current_user_token(username, password)
      if token['errorDetail'].present?
        return token
      else
        user_session_token = token[:session_token]
      end
      query({
        :endpoint => '/jsonsdk/SiteTraversal/searchSite',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token,
          :siteSearchString => "Australia"
        }
      })
    end


    def all_content_services
      query({
        :endpoint => '/jsonsdk/ContentServiceTraversal/getAllContentServices',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          :notrim => true
        }
      })
    end

  end
end