module Yodlee
  class Base

    include HTTParty

    cattr_accessor :current_app_session_token, :current_app_session_started

    def cobrand_token
      if fresh_app_token?
        YodleeUtil.log_yodlee_responses("Action : coblogin started | Cached token")
        token = current_app_session_token
        YodleeUtil.log_yodlee_responses("Action : coblogin started | Done")
        return token
      else
        YodleeUtil.log_yodlee_responses("Action : coblogin started")
        token = login_app
        YodleeUtil.log_yodlee_responses("Action : coblogin Ended")
        return token
      end
      # fresh_app_token? ? current_app_session_token : login_app
    end

    def current_user_token(username, password)
      user_session_login(username, password)
    end

    def public_key
      cobrand_public_key
    end

    def query opts
      YodleeUtil.log_yodlee_responses("#{'>' * 10} YODLEE API CALL : #{opts}  #{'<' * 10}\n")
      begin
        method   = opts[:method].to_s.downcase
        response = self.class.send(method, opts[:endpoint], query: opts[:params])
        data     = response.parsed_response
        YodleeUtil.log_yodlee_responses("#{'>' * 10} YODLEE API RESPONSE : #{response ? response.to_hash : ''} #{'<' * 10}\n")
        if response.success?
          if data.is_a?(Hash) and data['errorOccurred'] == "true"
            {'Error' => [{'errorDetail' => data['message'].present? ? data['message'] : data['detailedMessage']}]}
          elsif [ TrueClass, FalseClass, Fixnum ].include?(data.class)
            data
          else
            convert_to_mash(data)
          end
        else
          nil
        end
      rescue Errno::ETIMEDOUT => e
        return {'Error' => [{'errorDetail' => e.message}]}
      rescue => e
        opts[:response] = response
        return {'Error' => [{'errorDetail' => e.message}]}
      end
    end

    private
      def convert_to_mash data
        if data.is_a? Hash
          Hashie::Mash.new(data)
        elsif data.is_a? Array
          data.map { |d| Hashie::Mash.new(d) }
        end
      end

      def login_app
        response = query({
          :endpoint => '/authenticate/coblogin',
          :method => :POST,
          :params => {
            :cobrandLogin => Yodlee::Config.username,
            :cobrandPassword => Yodlee::Config.password
          }
        })
        if !response.present?
          e = "Error occured at yodlee login app. "
          e += "No response received. #{response}"
        elsif response["Error"].present?
          e = "Error occured at yodlee login app. "
          e += response["Error"][0]["errorDetail"]
        else
          self.current_app_session_started = Time.zone.now
          self.current_app_session_token = response.cobrandConversationCredentials.sessionToken
        end
      end

      def user_session_login(username, password)
        response = query({
          :endpoint => '/authenticate/login',
          :method => :POST,
          :params => {
            :cobSessionToken => cobrand_token,
            :login => username,
            :password => password,
          }
        })

        if response['Error']
          response
        else
          {:session_token => response.userContext.conversationCredentials.sessionToken}
        end
      end

      def cobrand_public_key
        url = "https://developer.api.yodlee.com/ysl/#{Yodlee::Config.username}/v1/cobrand/publicKey"
        header = {'Authorization' => "cobSession=#{cobrand_token}"}
        params = {:cobrandName => Yodlee::Config.username}
        response = self.class.send("get", url, query: params, headers: header)
      end

      def fresh_app_token?
        current_app_session_token && current_app_session_started && current_app_session_started >= 90.minutes.ago
      end
  end
end