module Yodlee
  module Fields
    class IfLogin < BaseField

      def input
        "<input class='string col-md-12 form-control noBorderRadius' id='#{name}' data-required='#{requirement}' name='#{name}' size='#{size}' type='text' maxlength='#{maxlength}' value='#{value}' autocomplete='off'/>"
      end
      
    end
  end
end