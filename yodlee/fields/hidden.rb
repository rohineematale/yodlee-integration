module Yodlee
  module Fields
    class Hidden < BaseField

      def input
        "<input class='string' name='#{name}' type='hidden' value='#{value}' />"
      end

    end
  end
end
