module Yodlee
  module Fields
    class BaseField

      attr_reader :field

      def initialize opts
        @field   = opts[:field]
        @wrapper = opts[:wrapper]
      end

      def render
        "
          <div class='col-md-8 nopaddingLeft' style='max-width:350px'>
            <label class='col-md-12 nopaddingLeft'>#{label} #{asterisk}</label>
            #{input}
          </div>
          <div class='clearfix'></div><br>
        "
      end

      def mfa_hidden_render
        "
          <div class='col-md-8 nopaddingLeft' style='max-width:350px'> #{input} </div>
          <div class='clearfix'></div>
        "
      end

      def label
        return field.displayName if field.displayName
        return field.question if field.question.present?
        return field.displayString if field.displayString.present?
      end

      def asterisk
        field.isOptional ? '' : '*'
      end

      def requirement
        field.isOptional ? 'optional' : 'required'
      end

      def required?
        !field.isOptional
      end

      def size
        field['size']
      end

      def maxlength
        field.maxlength
      end

      def value
        field.value
      end

      def img_url
        field.src
      end

      def name
        name = field.metaData if field.metaData.present?
        name = field.displayString if field.displayString.present?
        name = field.valueIdentifier if field.valueIdentifier
        if @wrapper.present?
          @wrapper + '[' + name + ']'
        else
          name
        end
      end

      def options
        str = "<option value=''>-Please Select-</option>"
        field['displayValidValues'].each_with_index do |ele, index|
          str += "<option value='#{field[:validValues][index]}'> #{ele}</option>"
        end
        return str
      end
    end
  end
end