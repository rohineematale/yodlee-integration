module Yodlee
  module Fields
    class Option < BaseField

      def input
        "<select class='string form-control' id='#{name}' data-required='#{requirement}' name='#{name}' maxlength='#{maxlength}'> #{options} </select>"
      end
      
    end
  end
end

