module Yodlee
  module Fields
    class Text < BaseField

      def input
        "<input class='string col-md-12 form-control noBorderRadius' id='#{name}' data-required='#{requirement}' name='#{name}' size='#{size}' type='text' maxlength='#{maxlength}' autocomplete='off' value='#{value}' />"
      end

    end
  end
end