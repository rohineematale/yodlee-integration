module Yodlee
  module Fields
    class IfPassword < BaseField

      def input
        "<input class='string col-md-12 form-control noBorderRadius' id='#{name}' data-required='#{requirement}' name='#{name}' size='#{size}' type='password' maxlength='#{maxlength}' value='#{value}' autocomplete='off'/>"
      end
      
    end
  end
end