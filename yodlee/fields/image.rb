module Yodlee
  module Fields
    class Image < BaseField

      def input
        "<img src='#{img_url}'>"
      end
      
    end
  end
end