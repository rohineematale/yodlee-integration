module Yodlee
  class User < Base

    def register_user(username, password, email)
      return cobrand_token if cobrand_token['Error'].present?
      query({
        :endpoint => '/jsonsdk/UserRegistration/register3',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          "userCredentials.loginName" => username,
          "userCredentials.password" => password,
          "userCredentials.objectInstanceType" => "com.yodlee.ext.login.PasswordCredentials",
          "userProfile.emailAddress" => email
        }
      })
    end

    def user_site_login(yodlee_account, site_id, user_session_token)
      params = {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token,
          :siteId => site_id,
          "credentialFields.enclosedType" => "com.yodlee.common.FieldInfoSingle"        
      }

      yodlee_account.keys.each_with_index do |key, index|
        params["credentialFields[#{index}].displayName"] = key
        params["credentialFields[#{index}].fieldType.typeName"] = "IF_LOGIN"
        params["credentialFields[#{index}].name"] = key
        params["credentialFields[#{index}].size"] = 20
        params["credentialFields[#{index}].value"] = yodlee_account[key]
        params["credentialFields[#{index}].valueIdentifier"] = key
        params["credentialFields[#{index}].valueMask"] = "LOGIN_FIELD"
        params["credentialFields[#{index}].isEditable"] = true
      end

      response = query({
        :endpoint => '/jsonsdk/SiteAccountManagement/addSiteAccount1',
        :method => :POST,
        :params => params
      })
    end

    def user_content_service_login(yodlee_account, content_service_id, user_session_token)

      return user_session_token if user_session_token['Error'].present?
      return cobrand_token if cobrand_token['Error'].present?

      params = {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token,
          :contentServiceId => content_service_id,
          "credentialFields.enclosedType" => "com.yodlee.common.FieldInfoSingle"        
      }

      yodlee_account.keys.each_with_index do |key, index|
        params["credentialFields[#{index}].displayName"] = key
        params["credentialFields[#{index}].fieldType.typeName"] = "TEXT"
        params["credentialFields[#{index}].name"] = key
        params["credentialFields[#{index}].size"] = 20
        params["credentialFields[#{index}].value"] = yodlee_account[key]
        params["credentialFields[#{index}].valueIdentifier"] = key
        params["credentialFields[#{index}].valueMask"] = "LOGIN_FIELD"
        params["credentialFields[#{index}].isEditable"] = true
      end

      params['shareCredentialsWithinSite'] = true
      params['startRefreshItemOnAddition'] = true

      response = query({
        :endpoint => '/jsonsdk/ItemManagement/addItemForContentService1',
        :method => :POST,
        :params => params
      })
    end

    def put_mfa_request_for_site(yodlee_account, memSiteAccId, user_session_token)
      return cobrand_token if cobrand_token['Error'].present?
      return user_session_token if user_session_token['Error'].present?
      BigfutureUtil.log_yodlee_responses("Action : putMFARequestForSite Started ")
      params = {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token,
          :memSiteAccId => memSiteAccId       
      }

      if (yodlee_account["mfa_type"] == "MFAQuesAns")
        params["userResponse.objectInstanceType"] = "com.yodlee.core.mfarefresh.MFAQuesAnsResponse"
        index = 0
        yodlee_account["security_questions"].each do |key, value|
          params["userResponse.quesAnsDetailArray[#{index}].answer"] = value["answer"]
          params["userResponse.quesAnsDetailArray[#{index}].answerFieldType"] = value["answerFieldType"]
          params["userResponse.quesAnsDetailArray[#{index}].metaData"] = key
          params["userResponse.quesAnsDetailArray[#{index}].question"] = value["question"]
          params["userResponse.quesAnsDetailArray[#{index}].questionFieldType"] = "label"
          index += 1
        end
      elsif (yodlee_account["mfa_type"] == "MFAToken")
        params["userResponse.objectInstanceType"] = "com.yodlee.core.mfarefresh.MFATokenResponse"
        params["userResponse.token"] = yodlee_account["token"]
      elsif (yodlee_account["mfa_type"] == "MFAImage")
        params["userResponse.objectInstanceType"] = "com.yodlee.core.mfarefresh.MFAImageResponse"
        params["userResponse.imageString"] = yodlee_account["image_text"]
      end
      response = query({
        :endpoint => '/jsonsdk/Refresh/putMFARequestForSite',
        :method => :POST,
        :params => params
      })
      BigfutureUtil.log_yodlee_responses("Action : putMFARequestForSite Ended")
      return response
    end

    def get_user_account_summary(user_session_token)
      query({
        :endpoint => '/account/summary/all',
        :method => :GET,
        :params => {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token
        }
      })
    end

    def get_user_info(user_session_token)
      query({
        :endpoint => '/jsonsdk/DataService/getItemSummariesForSite',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token
        }
      })
    end
  end
end