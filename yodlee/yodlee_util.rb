module Yodlee
  class YodleeUtil

    def self.log_yodlee_responses(messages)
      Rails.logger.info "#{'-'*40} #{messages} #{'-'*40}"
      Delayed::Worker.logger.add(Logger::INFO, "#{'-'*20} #{messages} #{'-'*20}")
    end

  end
end