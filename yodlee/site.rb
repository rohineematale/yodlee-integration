module Yodlee
  class Site < Base

    def login siteId
      return cobrand_token if cobrand_token['Error'].present?
      query({
        :endpoint => '/jsonsdk/SiteAccountManagement/getSiteLoginForm',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          :siteId => siteId
        }
      })
    end

    def get_all_site_accounts(user_session_token)
      return cobrand_token if cobrand_token['Error'].present?
      return user_session_token if user_session_token['Error'].present?
      query({
        :endpoint => '/jsonsdk/SiteAccountManagement/getAllSiteAccounts',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token
        }
      })
    end

    def get_site_refresh_info(memSiteAccId, user_session_token)
      return cobrand_token if cobrand_token['Error'].present?
      return user_session_token if user_session_token['Error'].present?
      YodleeUtil.log_yodlee_responses("Action : Get Site Refresh Info started ")
      response = query({
        :endpoint => '/jsonsdk/Refresh/getSiteRefreshInfo',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token,
          :memSiteAccId => memSiteAccId
        }
      })
      YodleeUtil.log_yodlee_responses("Action : Get Site Refresh Info Ended ")
      return response
    end

    def get_item_summaries_for_site(memSiteAccId, user_session_token)
      return cobrand_token if cobrand_token['Error'].present?
      return user_session_token if user_session_token['Error'].present?
      query({
        :endpoint => '/jsonsdk/DataService/getItemSummariesForSite',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token,
          :memSiteAccId => memSiteAccId
        }
      })
    end

    def get_mfa_site_que_ans(memSiteAccId, user_session_token)
      return cobrand_token if cobrand_token['Error'].present?
      return user_session_token if user_session_token['Error'].present?
      query({
        :endpoint => '/jsonsdk/SiteAccountManagement/getSiteAccountMfaQuestionsAndAnswers',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token,
          :memSiteAccId => memSiteAccId
        }
      })
    end

    def get_mfa_response_for_site(memSiteAccId, user_session_token)
      return cobrand_token if cobrand_token['Error'].present?
      return user_session_token if user_session_token['Error'].present?
      YodleeUtil.log_yodlee_responses("Action : Get MFA response started")
      response = query({
        :endpoint => '/jsonsdk/Refresh/getMFAResponseForSite',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token,
          :memSiteAccId => memSiteAccId
        }
      })

      if response[:fieldInfo] and response[:fieldInfo][:mfaFieldInfoType] == "IMAGE"
        binary_data = response.fieldInfo.image.pack('C*')
        rand_text = Time.now.to_i.to_s + rand.to_s[2..5]
        IO.binwrite("#{Rails.root}/public/captcha/#{rand_text}.bmp", binary_data)
        captcha = MfaCaptcha.create(:image => File.open("#{Rails.root}/public/captcha/#{rand_text}.bmp") )
        `rm -rf #{Rails.root}/public/captcha/*`
        response[:fieldInfo][:image] = captcha.image_url
      end
      YodleeUtil.log_yodlee_responses("Action : Get MFA response Ended")
      return response
    end

    def start_refresh_site(memSiteAccId, user_session_token)
      return cobrand_token if cobrand_token['Error'].present?
      return user_session_token if user_session_token['Error'].present?
      YodleeUtil.log_yodlee_responses("Action : Start Refresh started ")
      response = query({
        :endpoint => '/jsonsdk/Refresh/startSiteRefresh',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token,
          :memSiteAccId => memSiteAccId,
          "refreshParameters.refreshPriority" => 1,
          "refreshParameters.forceRefresh" => true
        }
      })
      YodleeUtil.log_yodlee_responses("Action : Start Refresh Ended ")
      return response
    end

    def remove_site_account(memSiteAccId, user_session_token)
      return cobrand_token if cobrand_token['Error'].present?
      return user_session_token if user_session_token['Error'].present?
      query({
        :endpoint => '/jsonsdk/SiteAccountManagement/removeSiteAccount',
        :method => :POST,
        :params => {
          :cobSessionToken => cobrand_token,
          :userSessionToken => user_session_token,
          :memSiteAccId => memSiteAccId
        }
      })
    end

    def change_site_credential(memSiteAccId, user_session_token, yodlee_account)
      return cobrand_token if cobrand_token['Error'].present?
      return user_session_token if user_session_token['Error'].present?

      params = {
        :cobSessionToken => cobrand_token,
        :userSessionToken => user_session_token,
        :memSiteAccId => memSiteAccId,
        "credentialFields.enclosedType" => "com.yodlee.common.FieldInfoSingle"        
      }

      yodlee_account.keys.each_with_index do |key, index|
        params["credentialFields[#{index}].displayName"] = key
        params["credentialFields[#{index}].fieldType.typeName"] = "IF_LOGIN"
        params["credentialFields[#{index}].name"] = key
        params["credentialFields[#{index}].value"] = yodlee_account[key]
        params["credentialFields[#{index}].valueIdentifier"] = key
        params["credentialFields[#{index}].valueMask"] = "LOGIN_FIELD"
      end

      query({
        :endpoint => '/jsonsdk/SiteAccountManagement/updateSiteAccountCredentials',
        :method => :POST,
        :params => params
      })
    end

  end
end